package com.example.penaltyminigame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.penaltyminigame.R
import com.example.penaltyminigame.util.Coordinate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Integer.min
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.sign
import kotlin.math.sqrt
import kotlin.random.Random

class PenaltyGameView  (context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var playerSize = 0.0f
    private var ballSize = 0.0f
    private var fieldSize = 1000
    private var goalSize = 100

    private val restartBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_baseline_replay)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val ballBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_football)?.toBitmap() ?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val keeperBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.keeper)
    private val gateBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.football_gate)
    private val playerFirstBitmap : Bitmap
    private val playerSecondBitmap : Bitmap

    private var cameraMidpoint = Coordinate(0.0f,0.0f)

    private var ballCoordinate = Coordinate(0.0f,0.0f)
    private var ballMoveVector = Coordinate(0.0f, 0.0f)

    private var arrowPoint = Coordinate(-1.0f, -1.0f)

    private var hitResults = arrayOf(0,0,0,0,0)
    private var computerResults = arrayOf(0,0,0,0,0)
    private var endScreenTimer = 0

    private var notification = ""
    private var notificationTimer = 0

    private var state = GAME

    init {
        val player = BitmapFactory.decodeResource(context.resources, R.drawable.player)
        val h = player.height
        playerFirstBitmap = Bitmap.createBitmap(player, 0,0,h,h, null, true )
        playerSecondBitmap = Bitmap.createBitmap(player, h,0,h,h, null, true )
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            playerSize = min(mWidth, mHeight) / 4.0f
            ballSize = playerSize / 2.0f
            fieldSize = mWidth * 2
            goalSize = (min(mWidth, mHeight) * 0.75f).toInt()
            cameraMidpoint = Coordinate(mWidth / 2.0f, mHeight / 2.0f)
            if (ballCoordinate.x == 0.0f && ballCoordinate.y == 0.0f) restart()
        }
    }

    companion object {
        const val CAMERA = 0
        const val GAME = 2
        const val END_SCREEN = 1
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state != END_SCREEN ) {
                updateBall()
                updateCamera()
            }
            drawField(it)
            drawBall(it)
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    GAME -> {
                        if(ballMoveVector.x == 0.0f && ballMoveVector.y == 0.0f) {
                            arrowPoint.x = event.x
                            arrowPoint.y = event.y
                        }
                    }
                    END_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    GAME -> {
                        if(ballMoveVector.x == 0.0f && ballMoveVector.y == 0.0f) {
                            arrowPoint.x = event.x
                            arrowPoint.y = event.y
                        }
                    }
                    END_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    GAME -> {
                        if(ballMoveVector.x == 0.0f && ballMoveVector.y == 0.0f) {
                            arrowPoint.x = event.x
                            arrowPoint.y = event.y
                            hitBall()
                        }
                    }
                    CAMERA -> {
                        cameraMidpoint.y = mHeight / 2.0f
                        cameraMidpoint.x = mWidth / 2.0f
                    }
                    END_SCREEN -> {
                        restart()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    ///  Public functions ///

    fun getState() : GameState = GameState(state, cameraMidpoint, Coordinate(mWidth / ballCoordinate.x, mHeight / ballCoordinate.y), ballMoveVector, arrowPoint
        , hitResults, computerResults, endScreenTimer, notification, notificationTimer)

    fun loadState(gameState: GameState) {
        state = gameState.state
        cameraMidpoint = gameState.camera
        ballCoordinate = Coordinate(mWidth * gameState.ball.x, mHeight * gameState.ball.y)
        ballMoveVector = gameState.ballMoveVector
        arrowPoint = gameState.arrow
        hitResults = gameState.hits
        computerResults = gameState.compHits
        endScreenTimer = gameState.endTimer
        notification = gameState.notification
        notificationTimer = gameState.notificationTimer
    }

    /// Private functions ///

    private fun drawField(c : Canvas) {
        val p = Paint()
        p.style = Paint.Style.FILL
        p.color = ResourcesCompat.getColor(context.resources, R.color.green, null)

        if(cameraMidpoint.y < mHeight / 10 * 9) c.drawRect(0.0f, mHeight - mHeight / 10 + (cameraMidpoint.y - mHeight / 2), mWidth.toFloat(), mHeight.toFloat(), p)
        if(cameraMidpoint.y in (mHeight / 20 - mHeight / 2.0f)..(playerSize + mHeight / 20 + mHeight / 2)
            && cameraMidpoint.x in (mWidth / 10 - mWidth / 2.0f)..(mWidth / 10 + mWidth / 2 + playerSize)) {
            val left = mWidth / 10  + mWidth / 2.0f - cameraMidpoint.x
            val top = mHeight - mHeight / 20 - playerSize.toInt() - mHeight / 2 + cameraMidpoint.y
            c.drawBitmap(if(ballMoveVector.y == 0.0f && ballMoveVector.x == 0.0f) playerFirstBitmap else playerSecondBitmap,
                null,
                Rect(left.toInt(), top.toInt(), (left + playerSize).toInt(), (top + playerSize).toInt()), p)
        }
        if(cameraMidpoint.y in (mHeight / 20 - mHeight / 2.0f)..(playerSize + mHeight / 20 + mHeight / 2)
            && cameraMidpoint.x in (mWidth * 2 - mWidth / 10 - playerSize - mWidth / 2.0f)..(mWidth * 2 - mWidth / 10 + mWidth / 2.0f)) {
            val w = playerSize / (keeperBitmap.height / keeperBitmap.width)
            val left = (mWidth - mWidth / 10 - w).toInt() - (cameraMidpoint.x - (mWidth * 1.5f)).toInt()
            val top = mHeight - mHeight / 20 - playerSize.toInt() - mHeight / 2 + cameraMidpoint.y
            c.drawBitmap(keeperBitmap, null,
                Rect(left, top.toInt(), (left + w).toInt(), (top + playerSize).toInt()), p)
        }
        if(cameraMidpoint.y in (mHeight / 20 - mHeight / 2.0f)..(goalSize + mHeight / 20 + mHeight / 2.0f)
            && cameraMidpoint.x in (mWidth * 2 - mWidth / 10 - mWidth / 2.0f)..(mWidth * 2 - mWidth / 10 + goalSize * (5 / 6) + mWidth / 2.0f)) {
            val left = (mWidth - mWidth / 10) - (cameraMidpoint.x - mWidth * 1.5f).toInt()
            val top = mHeight - mHeight / 10 - goalSize - mHeight / 2 + cameraMidpoint.y + goalSize / 4
            c.drawBitmap(gateBitmap, null,
                Rect(left, top.toInt(), (left + goalSize * (5.0f / 6.0f)).toInt(), ((top + goalSize).toInt())), p)
        }
    }

    private fun drawUI(c : Canvas) {
        val p = Paint()
        p.style = Paint.Style.FILL
        p.strokeWidth = 3.0f
        p.textAlign = Paint.Align.CENTER
        when(state) {
            CAMERA -> {
                for(n in hitResults.indices) {
                    when(hitResults[n]) {
                        -1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.RED
                        }
                        0 -> {
                            p.style = Paint.Style.STROKE
                            p.color = Color.WHITE
                        }
                        1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.GREEN
                        }
                    }
                    c.drawCircle(mWidth / 2.0f - mWidth / 4.0f + (n + 0.5f) * (mWidth / 10), 50.0f, 25.0f, p)
                }
            }
            GAME -> {
                for(n in hitResults.indices) {
                    when(hitResults[n]) {
                        -1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.RED
                        }
                        0 -> {
                            p.style = Paint.Style.STROKE
                            p.color = Color.WHITE
                        }
                        1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.GREEN
                        }
                    }
                    c.drawCircle(mWidth / 2.0f - mWidth / 4.0f + (n + 0.5f) * (mWidth / 10), 50.0f, 25.0f, p)
                }
                drawArrow(c)
            }
            END_SCREEN -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                val size = kotlin.math.min(mWidth, mHeight) * 0.8f
                c.drawRoundRect((mWidth - size) / 2.0f,
                    mHeight  * 0.1f,
                    (mWidth + size) * 0.5f, mHeight  * 0.9f, 20f, 20f, p)
                p.color = Color.YELLOW
                p.style = Paint.Style.STROKE
                c.drawRoundRect((mWidth - size) / 2.0f,
                    mHeight  * 0.1f,
                    (mWidth + size) * 0.5f, mHeight  * 0.9f, 20f, 20f, p)

                p.textSize = 40.0f
                p.style = Paint.Style.FILL
                p.color = Color.WHITE
                c.drawText("Ваш результат", mWidth / 2.0f, mHeight * 0.15f, p)
                for(n in hitResults.indices) {
                    when(hitResults[n]) {
                        -1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.RED
                        }
                        0 -> {
                            p.style = Paint.Style.STROKE
                            p.color = Color.WHITE
                        }
                        1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.GREEN
                        }
                    }
                    c.drawCircle(mWidth / 2.0f - (size - 20) / 2.0f + (n + 0.5f) * ((size - 20) / 5f), mHeight * 0.15f + 50, 20.0f, p)
                }

                endScreenTimer++
                when(endScreenTimer) {
                    1 -> computerResults[0] = Random.nextInt(2)
                    30 -> computerResults[1] = Random.nextInt(2)
                    60 -> computerResults[2] = Random.nextInt(2)
                    90 -> computerResults[3] = Random.nextInt(2)
                    120 -> computerResults[4] = Random.nextInt(2)
                }

                p.color = Color.WHITE
                c.drawText("Результат противника", mWidth / 2.0f, mHeight * 0.15f + 100, p)
                for(n in computerResults.indices) {
                    when(computerResults[n]) {
                        0 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.RED
                            c.drawCircle(mWidth / 2.0f - (size - 20) / 2.0f + (n + 0.5f) * ((size - 20) / 5f), mHeight * 0.15f + 150, 20.0f, p)
                        }
                        1 -> {
                            p.style = Paint.Style.FILL
                            p.color = Color.GREEN
                            c.drawCircle(mWidth / 2.0f - (size - 20) / 2.0f + (n + 0.5f) * ((size - 20) / 5f), mHeight * 0.15f + 150, 20.0f, p)
                        }
                    }
                }

                val restartSize = (mHeight * 0.9f - mHeight * 0.15f - 180) / 2
                if(endScreenTimer > 120) {
                    p.color = Color.WHITE
                    var yR = 0
                    for(h in hitResults) if(h > 0) yR++
                    c.drawText("$yR - ${computerResults.sum()}", mWidth / 2.0f, mHeight * 0.9f - restartSize - 80, p)
                    val res = if(yR > computerResults.sum()) {
                        p.color = Color.GREEN
                        "Победа"
                    }
                    else if(yR == computerResults.sum()) {
                        p.color = Color.WHITE
                        "Ничья"
                    }
                    else {
                        p.color = Color.RED
                        "Поражение"
                    }
                    c.drawText(res, mWidth / 2.0f, mHeight * 0.9f - restartSize - 40, p)
                }

                c.drawBitmap(restartBitmap, null, Rect(((mWidth - restartSize) / 2).toInt(),
                    (mHeight * 0.9f - restartSize - 10).toInt(),
                    ((mWidth + restartSize) / 2).toInt(), (mHeight * 0.9f - 10).toInt()), p)
            }
        }
        if(state != END_SCREEN && notificationTimer > 0) {
            notificationTimer--
            p.isFakeBoldText = true
            p.color = ResourcesCompat.getColor(context.resources, R.color.light_grey, null)
            val width = notification.length * 30 + 20
            c.drawRoundRect((mWidth - width) / 2.0f, mHeight  * 0.2f - 25,
                (mWidth + width) * 0.5f, mHeight * 0.2f + 25, 10f, 10f, p)
            p.color = Color.YELLOW
            p.style = Paint.Style.STROKE
            c.drawRoundRect((mWidth - width) / 2.0f, mHeight  * 0.2f - 25,
                (mWidth + width) * 0.5f, mHeight * 0.2f + 25, 10f, 10f, p)

            p.textSize = 40.0f
            p.style = Paint.Style.FILL
            p.color = Color.WHITE
            c.drawText(notification, mWidth / 2.0f, mHeight * 0.2f + 10, p)
        }
    }

    private fun drawArrow(c: Canvas) {
        if(arrowPoint.x > mWidth / 10 + playerSize + ballSize / 2 && arrowPoint.y in 0.0f..(mHeight - mHeight / 20 - playerSize / 2)) {
            val p = Paint()
            p.color = Color.WHITE
            p.style = Paint.Style.FILL
            val vX = arrowPoint.x - mWidth / 10 - playerSize - ballSize / 2
            val vY = mHeight - arrowPoint.y - mHeight / 20 - playerSize / 2
            var nX = 1.0f
            var nY = -vX / vY
            val len = sqrt(nX.pow(2) + nY.pow(2))
            nX *= 2 / len
            nY *= 2 / len
            val aLine = Path().also {
                it.moveTo(mWidth / 10 + playerSize + ballSize / 2 - nX, mHeight - mHeight / 20 - playerSize / 2 + nY)
                it.lineTo(arrowPoint.x - vX * 0.1f - nX, arrowPoint.y + vY * 0.1f + nY)
                it.lineTo(arrowPoint.x - vX * 0.1f + nX, arrowPoint.y + vY * 0.1f - nY)
                it.lineTo(mWidth / 10 + playerSize + ballSize / 2 + nX, mHeight - mHeight / 20 - playerSize / 2 - nY)
                it.close()
            }
            val aEnd = Path().also {
                it.moveTo(arrowPoint.x - vX * 0.1f - nX * 5, arrowPoint.y + vY * 0.1f + nY * 5)
                it.lineTo(arrowPoint.x, arrowPoint.y)
                it.lineTo(arrowPoint.x - vX * 0.1f + nX * 5, arrowPoint.y + vY * 0.1f - nY * 5)
                it.close()
            }
            c.drawPath(aLine, p)
            c.drawPath(aEnd, p)
        }
    }

    private fun drawBall(c : Canvas) {
        when(state) {
            CAMERA -> {
                if(cameraMidpoint.x in (ballCoordinate.x - mWidth / 2)..(ballCoordinate.x + ballSize + mWidth / 2) &&
                        cameraMidpoint.y in (-(ballCoordinate.y - mHeight) - ballSize - mHeight / 2)..(-(ballCoordinate.y - mHeight) + mHeight / 2)) {
                    val left = ballCoordinate.x + mWidth / 2.0f - cameraMidpoint.x
                    val top = ballCoordinate.y - mHeight / 2.0f + cameraMidpoint.y
                    c.drawBitmap(ballBitmap, null, Rect(left.toInt(), top.toInt(),
                        (left + ballSize).toInt(), (top + ballSize).toInt()), Paint())
                }
            }
            else -> {
                val y = if(ballCoordinate.y <= mHeight / 10) (mHeight / 10) else ballCoordinate.y.toInt()
                val x = when(ballCoordinate.x) {
                    in 0.0f..(mWidth / 2.0f) -> ballCoordinate.x
                    in (mWidth / 2.0f)..(mWidth * 1.5f) -> mWidth / 2
                    else -> ballCoordinate.x - mWidth
                }
                c.drawBitmap(ballBitmap, null, Rect(x.toInt(), y,
                    (x.toInt() + ballSize).toInt(), y + ballSize.toInt()), Paint())
            }
        }
    }

    private fun updateBall() {
        if(ballCoordinate.y + ballMoveVector.y > mHeight - mHeight / 20 - ballSize) {
            ballCoordinate.y = mHeight - mHeight / 20 - ballSize
            ballMoveVector.y = 0.0f
        } else {
            ballCoordinate.y += ballMoveVector.y
            if(ballMoveVector.y < 5.0f && ballMoveVector.y != 0.0f) {
                ballMoveVector.y += 0.35f
                if(ballMoveVector.y == 0.0f) ballMoveVector.y = 0.00001f
            }
        }
        val w = playerSize / (keeperBitmap.height / keeperBitmap.width)
        if(ballCoordinate.x in (mWidth * 2 - mWidth / 10 - w - ballSize - ballMoveVector.x)..(mWidth * 2 - mWidth / 10 - w - ballSize) &&
                ballCoordinate.y in (mHeight - mHeight / 20 - ballSize - playerSize)..(mHeight - mHeight / 20 - ballSize)) {
            ballCoordinate.x = mWidth * 2 - mWidth / 10 - w - ballSize
            miss()
        }
        if(ballCoordinate.x in (mWidth * 2 - mWidth / 10 - ballSize - ballMoveVector.x)..(mWidth * 2 - mWidth / 10 - ballSize) &&
            ballCoordinate.y in (mHeight - mHeight / 10.0f - goalSize + goalSize / 4)..(mHeight - ballSize)) {
            goal()
        }
        if(ballCoordinate.x in (mWidth * 2 - mWidth / 10 - ballSize - ballMoveVector.x)..(mWidth * 2 - mWidth / 10 - ballSize) &&
            ballCoordinate.y in (mHeight - mHeight / 10.0f - goalSize - ballSize + goalSize / 4)..(mHeight - mHeight / 10.0f + goalSize / 4 - goalSize - 1)) {
            ballMoveVector.x *= -1
        }
        if(ballCoordinate.x !in (-ballSize)..(mWidth * 2.0f + 10)) miss()

        ballCoordinate.x += ballMoveVector.x
    }

    private fun updateCamera() {
        when(state) {
            GAME -> {
                cameraMidpoint.x = ballCoordinate.x.coerceAtLeast(mWidth / 2.0f).coerceAtMost(mWidth * 1.5f)
                cameraMidpoint.y = (-(ballCoordinate.y + mHeight)).coerceAtLeast(mHeight / 2.0f)
            }
            CAMERA -> {
                val dX = mWidth / 30 * (mWidth / 2 - cameraMidpoint.x).sign
                val dY = mHeight / 30 * (mHeight / 2 - cameraMidpoint.y).sign
                cameraMidpoint.y = if(cameraMidpoint.y + dY in (mHeight / 2.0f - dY.absoluteValue * 2)..(mHeight / 2.0f + dY.absoluteValue * 2)) mHeight / 2.0f else cameraMidpoint.y + dY
                cameraMidpoint.x = if(cameraMidpoint.x + dX in (mWidth / 2.0f - dX.absoluteValue * 2)..(mWidth / 2.0f + dX.absoluteValue * 2)) mWidth / 2.0f else cameraMidpoint.x + dX
                if(cameraMidpoint.x == mWidth / 2.0f && cameraMidpoint.y == mHeight / 2.0f) state = GAME
            }
        }

    }

    private fun goal() {
        ballMoveVector.x = 0.0f
        ballMoveVector.y = 0.0f
        ballCoordinate.x = mWidth / 10 + playerSize
        ballCoordinate.y = mHeight - mHeight / 20.0f - (playerSize + ballSize) / 2
        notification = "Гол"
        notificationTimer = 60
        val last = hitResults.indexOf(0)
        if(last >= 0) {
            hitResults[last] = 1
            state = if(last == 4) END_SCREEN
            else CAMERA
        }
    }

    private fun miss() {
        ballMoveVector.x = 0.0f
        ballMoveVector.y = 0.0f
        ballCoordinate.x = mWidth / 10 + playerSize
        ballCoordinate.y = mHeight - mHeight / 20.0f - (playerSize + ballSize) / 2
        notification = "Неудача"
        notificationTimer = 60
        val last = hitResults.indexOf(0)
        if(last >= 0) {
            hitResults[last] = -1
            state = if(last == 4) END_SCREEN
            else CAMERA
        }
    }

    private fun hitBall() {
        val vX = arrowPoint.x
        val vY = -(mHeight - arrowPoint.y)
        val one = min(mWidth, mHeight) / 20
        ballMoveVector.x = vX / one
        ballMoveVector.y = vY / one
        arrowPoint.x = -1.0f
        arrowPoint.y = -1.0f
        if(ballMoveVector.x < mWidth / 20) ballMoveVector.x = mWidth / 20.0f
    }

    private fun restart() {
        cameraMidpoint.x = mWidth * 1.5f
        cameraMidpoint.y = mHeight / 2.0f
        state = CAMERA
        for(n in 0..4) {
            hitResults[n] = 0
            computerResults[n] = -1
        }
        ballCoordinate.x = mWidth / 10 + playerSize
        ballCoordinate.y = mHeight - mHeight / 20.0f - (playerSize + ballSize) / 2
        ballMoveVector.x = 0.0f
        ballMoveVector.y = 0.0f
        endScreenTimer = 0
        notificationTimer = 0
    }

}