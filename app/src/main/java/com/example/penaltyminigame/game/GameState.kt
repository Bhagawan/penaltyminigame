package com.example.penaltyminigame.game

import com.example.penaltyminigame.util.Coordinate

data class GameState(val state : Int,
                     val camera : Coordinate,
                     val ball: Coordinate,
                     val ballMoveVector: Coordinate,
                     val arrow: Coordinate
                     , val hits: Array<Int>
                     , val compHits: Array<Int>
                     , val endTimer: Int
                     , val notification : String
                     , val notificationTimer : Int) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameState

        if (state != other.state) return false
        if (camera != other.camera) return false
        if (ball != other.ball) return false
        if (ballMoveVector != other.ballMoveVector) return false
        if (arrow != other.arrow) return false
        if (!hits.contentEquals(other.hits)) return false
        if (!compHits.contentEquals(other.compHits)) return false
        if (endTimer != other.endTimer) return false
        if (notification != other.notification) return false
        if (notificationTimer != other.notificationTimer) return false

        return true
    }

    override fun hashCode(): Int {
        var result = state
        result = 31 * result + camera.hashCode()
        result = 31 * result + ball.hashCode()
        result = 31 * result + ballMoveVector.hashCode()
        result = 31 * result + arrow.hashCode()
        result = 31 * result + hits.contentHashCode()
        result = 31 * result + compHits.contentHashCode()
        result = 31 * result + endTimer
        result = 31 * result + notification.hashCode()
        result = 31 * result + notificationTimer
        return result
    }
}
