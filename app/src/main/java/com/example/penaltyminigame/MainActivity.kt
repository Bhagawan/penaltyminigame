package com.example.penaltyminigame

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.penaltyminigame.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.launch
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var target : Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        loadBackGround()
        setContentView(binding.root)
    }

    override fun onResume() {
        lifecycleScope.launch {
            viewModel.gameFlow.collect {
                binding.game.loadState(it)
            }
        }
        super.onResume()
    }

    override fun onPause() {
        viewModel.saveState(binding.game.getState())
        super.onPause()
    }

    private fun loadBackGround() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let {
                    binding.root.background = BitmapDrawable(resources, it)
                }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
        }
        Picasso.get().load("http://195.201.125.8/PenaltyMinigame/back.png").into(target)
    }
}