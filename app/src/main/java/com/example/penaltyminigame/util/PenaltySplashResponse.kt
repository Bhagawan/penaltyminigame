package com.example.penaltyminigame.util

import androidx.annotation.Keep

@Keep
data class PenaltySplashResponse(val url : String)