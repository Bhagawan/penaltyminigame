package com.example.penaltyminigame.util

data class Coordinate(var x : Float, var y : Float)